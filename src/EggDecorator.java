public interface EggDecorator extends Egg{
    void decorate();

}
class ColorDecorator implements EggDecorator {
    private final Egg egg;

    public ColorDecorator(Egg egg) {
        this.egg = egg;
    }

    @Override
    public void create() {
        egg.create();
    }

    @Override
    public void decorate() {
        System.out.println("Coloring the egg");
    }
}

class StickerDecorator implements EggDecorator {
    private final Egg egg;

    public StickerDecorator(Egg egg) {
        this.egg = egg;
    }

    @Override
    public void create() {
        egg.create();
    }

    @Override
    public void decorate() {
        System.out.println("Adding stickers to the egg");
    }
}

