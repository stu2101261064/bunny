public class Main {
    public static void main(String[] args) {
        EasterBunny easterBunny = EasterBunny.getInstance();

        EggFactory eggFactory = new EggFactory();
        Egg chickenEgg = eggFactory.createEgg("chicken");
        Egg ostrichEgg = eggFactory.createEgg("ostrich");
        Egg dinosaurEgg = eggFactory.createEgg("dinosaur");

        EggDecorator coloredChickenEgg = new ColorDecorator(chickenEgg);
        EggDecorator stickeredOstrichEgg = new StickerDecorator(ostrichEgg);

        HideStrategy basketHideStrategy = new BasketHideStrategy();
        HideStrategy forestHideStrategy = new ForestHideStrategy();
        HideStrategy bushHideStrategy = new BushHideStrategy();

        coloredChickenEgg.create();
        coloredChickenEgg.decorate();
        basketHideStrategy.hide();

        stickeredOstrichEgg.create();
        stickeredOstrichEgg.decorate();
        forestHideStrategy.hide();

        dinosaurEgg.create();
        bushHideStrategy.hide();
    }
}
