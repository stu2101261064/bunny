interface Egg {
    void create();

}
class ChickenEgg implements Egg {
    @Override
    public void create() {
        System.out.println("Creating a chicken egg");
    }
}

class OstrichEgg implements Egg {
    @Override
    public void create() {
        System.out.println("Creating an ostrich egg");
    }
}

class DinosaurEgg implements Egg {
    @Override
    public void create() {
        System.out.println("Creating a dinosaur egg");
    }
}

class EggFactory {
    public Egg createEgg(String type) {
        return switch (type) {
            case "chicken" -> new ChickenEgg();
            case "ostrich" -> new OstrichEgg();
            case "dinosaur" -> new DinosaurEgg();
            default -> null;
        };
    }
}
