public interface HideStrategy {
    void hide();
}
class BasketHideStrategy implements HideStrategy {
    @Override
    public void hide() {
        System.out.println("Hiding the egg in a basket");
    }
}

class ForestHideStrategy implements HideStrategy {
    @Override
    public void hide() {
        System.out.println("Hiding the egg in the forest");
    }
}

class BushHideStrategy implements HideStrategy {
    @Override
    public void hide() {
        System.out.println("Hiding the egg under a bush");
    }
}